package hust.soict.hedspi.aims.media;

import java.util.Scanner;

public class Disc extends Media {
	private String director;
	private int length;
	
	
	
	public String getDirector() {
		return director;
	}
	public int getLength() {
		return length;
	}
	public Disc() {
		// TODO Auto-generated constructor stub
	}
	public void createDisc() {
		super.createMedia();
		Scanner sc = new Scanner(System.in);
		System.out.println("Input Director: ");
		this.director = sc.nextLine();
		System.out.println("Input the length: ");
		this.length = sc.nextInt();
	}
	
	public Disc(String title) {
		super(title);
	}
	public Disc(String title, String category,float cost, String director, int length) {
		super(title, category, cost);
		this.director = director;
		this.length = length;
	}
	
	public String toString() {
		return super.toString() +" - Director: "+ this.director+" - Length: " + this.length;
	}

}

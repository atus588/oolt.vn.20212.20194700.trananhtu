package hust.soict.hedpsi.aims.utils;


public class DateUtils {
	
	public int compareDate(MyDate date1, MyDate date2) {
		if (date1.getYear()<date2.getYear()) {
			return -1;
		} 
		else if (date1.getYear()>date2.getYear()) {
			return 1;
		}
		else {
			if (date1.getMonth()<date2.getMonth()) {
				return -1;
			} 
			else if (date1.getMonth()>date2.getMonth()) {
				return 1;
			}
			else 
			{
				if (date1.getDay()<date2.getDay()) {
					return -1;
				} 
				else if (date1.getDay()>date2.getDay()) {
					return 1;
				}
				else return 0;
			}
		}
	}
	
	public void sortDate (MyDate...dates) {
		for (int i = 0; i<dates.length;i++) {
			for (int j = i+1;j<dates.length; j++) {
				int k = compareDate(dates[i],dates[j]);
//				System.out.println(k);
				if (k==-1) {
					swapDate(dates[i],dates[j]);
				}
			}
		}
		for (int i = 0; i<dates.length;i++)
			dates[i].printProperly(dates[i]);
	}
	 public static void swapDate(MyDate date1, MyDate date2) {
		 	MyDate tmp = new MyDate();
//	        swap
		 	tmp.setDay(date1.getDay());
		 	tmp.setMonth(date1.getMonth());
		 	tmp.setYear(date1.getYear());
	        date1.setDay(date2.getDay());
	        date1.setMonth(date2.getMonth());
	        date1.setYear(date2.getYear());
	        date2.setDay(tmp.getDay());
	        date2.setMonth(tmp.getMonth());
	        date2.setYear(tmp.getYear());
	    }


}

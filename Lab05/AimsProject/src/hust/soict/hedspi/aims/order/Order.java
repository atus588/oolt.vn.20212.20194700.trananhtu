package hust.soict.hedspi.aims.order;
import hust.soict.hedpsi.aims.utils.MyDate;
import hust.soict.hedspi.aims.disc.DigitalVideoDisc;

public class Order<dateOrdered> {
	public static final int MAX_NUMBER_ORDERED = 10;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
	int qtyOrdered;
	float total;
	
	private MyDate dateOrdered = new MyDate();
	public static final int MAX_LIMITED_ORDERS = 5;
	private static int nbOrders = 0;
	
	private Order() {
		
	}
	
	public static Order createOrder() {
		if (nbOrders >= MAX_LIMITED_ORDERS)
		{
			System.out.println("You have reached maximum orders");
			return null;
		}
		else {
			nbOrders ++;
			return new Order();
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered <MAX_NUMBER_ORDERED) {
		 itemsOrdered[qtyOrdered]= disc;
		 qtyOrdered++;
		 System.out.println(disc.getTitle()+" has been added!");
		}
		else System.out.println("The order is full! Cannot add "+disc.getTitle());
	 } 
	

	
//	public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList) {
//		for (int i=0;i<dvdList.length;i++) {
//			if (qtyOrdered < MAX_NUMBER_ORDERED) {
//				itemsOrdered[qtyOrdered] = dvdList[i];
//				qtyOrdered++;
//			} else {
//				System.out.println("Order maxed. Cannot add: "+dvdList[i]);
//			}
//		}
//	}
	
	
	public void addDigitalVideoDisc(DigitalVideoDisc... dvdList) {
		for (int i=0;i<dvdList.length;i++) {
			if (qtyOrdered < MAX_NUMBER_ORDERED) {
				itemsOrdered[qtyOrdered] = dvdList[i];
				System.out.println(dvdList[i].getTitle()+" has been added!");
				qtyOrdered++;
			} else {
				System.out.println("Order maxed. Cannot add: "+dvdList[i].getTitle());
			}
		}
} 
	
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int check=0;
		for (int i=0;i<=qtyOrdered;i++) {
			if (itemsOrdered[i]==disc) {
				check=1;
				for (int j=i;j<qtyOrdered;j++) {
					itemsOrdered[j]=itemsOrdered[j+1];
				}
				itemsOrdered[qtyOrdered]=null;
				qtyOrdered--;
				System.out.println("The item has been removed!");
				break;
			}
		}
		if (check == 0) System.out.println("That item not in your order!");	
	}
	
	
	public void printOrder() {
		for (int i=0;i<qtyOrdered;i++) {
			System.out.println(itemsOrdered[i]);
		}
	}
	
	
	public float totalCost() {
		for (int i=0;i<qtyOrdered;i++) {
			total += itemsOrdered[i].getCost();
		}
		return total;
	}
	public void newPrintOrder() {
		System.out.println("*************Order****************");
		System.out.print("Date: ");dateOrdered.getOrderedDate();
		System.out.println("Ordered Items");
		for (int i=0; i<itemsOrdered.length;i++) {
			System.out.println(i+1+". DVD - "+itemsOrdered[i].getTitle()+" - "+itemsOrdered[i].getCategory()+" - "
					+itemsOrdered[i].getDirector()+" - "+itemsOrdered[i].getLength()+" : "+itemsOrdered[i].getCost());
		}
		System.out.println("Total cost: "+totalCost());
		System.out.println("**********************************");
		
	}
	
	public void getALuckyItem() {
		int randRange = qtyOrdered - 1;
//		System.out.println(qtyOrdered);
//		System.out.println(randRange);
		int randNum = (int)(Math.random()*randRange);
//		System.out.println(randNum);
		System.out.println("The lucky items is: "+(randNum+1)+" - "+itemsOrdered[randNum].getTitle());
		System.out.println("New total cost: "+ (this.total - itemsOrdered[randNum].getCost()));
	}
	
}

package hust.soict.hedspi.lab01;
import javax.swing.JOptionPane;
import java.lang.Math;

public class SecondDegreeEq {
    public static void main (String[] args){
        double a, b, c, x1, x2, delta; 
        String notif3 = "𝑎𝑥^2 +𝑏𝑥 + 𝑐 = 0";
        a = Double.parseDouble(JOptionPane.showInputDialog(notif3 +"\nEnter a: ")) ;
        b = Double.parseDouble(JOptionPane.showInputDialog(notif3 +"\nEnter b: ")) ;
        c = Double.parseDouble(JOptionPane.showInputDialog(notif3 +"\nEnter c: ")) ;

        if( a == 0 ) {
            if( b == 0){
                if( c == 0) {
                    JOptionPane.showMessageDialog(null, "The equation has infinitely many solutions!");
                    System.exit(0);
                } else {
                    JOptionPane.showMessageDialog(null, "The equation has no solution!");
                    System.exit(0);
                }
            } else {
                JOptionPane.showMessageDialog(null, "The equation has a unique solution 𝑥1 = " + (-c/b));
                System.exit(0);
            }
        }
        delta = b*b - 4*a*c;
        if(delta == 0) {
            JOptionPane.showMessageDialog(null, "The equation has double root 𝑥1 = 𝑥2 = " + (-b/(2*a)));
        }
        else if(delta > 0){
            x1 = ((-b + Math.sqrt(delta)) / (2*a));
            x2 = ((-b - Math.sqrt(delta)) / (2*a));
            JOptionPane.showMessageDialog(null, "The equation has two distinct roots (𝑥1, 𝑥2) = (" + x1 + ", " + x2 + ")");

        }else{
            JOptionPane.showMessageDialog(null, "The equation has no solution!");
        }

        }

}

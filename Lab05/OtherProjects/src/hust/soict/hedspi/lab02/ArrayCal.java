package hust.soict.hedspi.lab02;
import java.util.Arrays;
import java.util.Scanner;
public class ArrayCal
{
    public static void main(String[] args) 
    {
        int n, sum = 0;
        float average;
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter number of elements you want in array:");
        n = keyboard.nextInt();
        int array[] = new int[n];
        System.out.println("Enter all the elements:");
        for(int i = 0; i < n ; i++)
        {
            array[i] = keyboard.nextInt();
            sum = sum + array[i];
        }
        System.out.println("Sum:"+sum);
        average = (float)sum / n;
        System.out.println("Average:"+average);
        Arrays.sort(array);
        for (int i = 0; i < array.length; i++)   
        {       
        System.out.print(array[i]+" ");   
        }   
    }
}

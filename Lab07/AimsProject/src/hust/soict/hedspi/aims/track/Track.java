package hust.soict.hedspi.aims.track;

import java.util.Scanner;

public class Track implements Playable {
	private String title;
	private int length;
	
	public Track() {
		// TODO Auto-generated constructor stub
		super();
	}
	public Track(String title, int length) {
		super();
		this.title = title;
		this.length = length;
	}

	public String getTitle() {
		return title;
	}

	public int getLength() {
		return length;
	}
//---------------------------------------------------------------------
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	public static Track createTrack() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter title: ");
		String temp = sc.nextLine();
		System.out.println("Enter length: ");
		int tempN = sc.nextInt();
		return new Track(temp,tempN);
	}

}

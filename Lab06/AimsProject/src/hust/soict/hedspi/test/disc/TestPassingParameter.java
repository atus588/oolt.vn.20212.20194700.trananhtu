package hust.soict.hedspi.test.disc;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;

public class TestPassingParameter {
	
	public static void main(String[] args) {
//		DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle","Jungle","Athur",90,24.4f);
//		DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella","Princess","Disney",120,5f);
//		
//		swap(jungleDVD, cinderellaDVD);
//		System.out.println("jungle DVD title: " + jungleDVD.getTitle());
//		System.out.println("cinderella DVD title: " + cinderellaDVD.getTitle());
//		
////		changeTitle(jungleDVD, cinderellaDVD.getTitle());
////		System.out.println("jungle DVD title: "+ jungleDVD.getTitle());
//		swapDVD(jungleDVD,cinderellaDVD);
//		System.out.println("jungle DVD: " + jungleDVD.getTitle()+jungleDVD.getCategory()+jungleDVD.getDirector()+jungleDVD.getLength()+jungleDVD.getCost());
//		System.out.println("cinderella DVD: " + cinderellaDVD.getTitle()+cinderellaDVD.getCategory()+cinderellaDVD.getDirector()+cinderellaDVD.getLength()+cinderellaDVD.getCost());
////		swapTitle(jungleDVD,cinderellaDVD);
	}
	
	
	public static void swap(Object o1, Object o2) {
		Object tmp = o1;
		o1 = o2;
		o2 = tmp;
	}
	public static void changeTitle(DigitalVideoDisc dvd, String title) {
		String oldTitle = dvd.getTitle();
		dvd.setTitle(title);
		dvd = new DigitalVideoDisc();
		dvd.setTitle(oldTitle);
	}
	public static void swapTitle(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		DigitalVideoDisc tmp = new DigitalVideoDisc();
		tmp.setTitle(dvd1.getTitle());
//		System.out.println(tmp.getTitle());
		dvd1.setTitle(dvd2.getTitle());
//		System.out.println(dvd1.getTitle());
		dvd2.setTitle(tmp.getTitle());
//		System.out.println(dvd2.getTitle());
	}
	public static void swapCategory(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		DigitalVideoDisc tmp = new DigitalVideoDisc();
		tmp.setCategory(dvd1.getCategory());
		dvd1.setCategory(dvd2.getCategory());
		dvd2.setCategory(tmp.getCategory());
	}
	public static void swapDirector(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		DigitalVideoDisc tmp = new DigitalVideoDisc();
		tmp.setDirector(dvd1.getDirector());
		dvd1.setDirector(dvd2.getDirector());
		dvd2.setDirector(tmp.getDirector());

	}
	public static void swapLength(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		DigitalVideoDisc tmp = new DigitalVideoDisc();
		tmp.setLength(dvd1.getLength());
		dvd1.setLength(dvd2.getLength());
		dvd2.setLength(tmp.getLength());
	}
	public static void swapCost(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		DigitalVideoDisc tmp = new DigitalVideoDisc();
		tmp.setCost(dvd1.getCost());
		dvd1.setCost(dvd2.getCost());
		dvd2.setCost(tmp.getCost());
	}
	public static void swapDVD(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		swapTitle(dvd1,dvd2);
		swapCategory(dvd1,dvd2);
		swapDirector(dvd1,dvd2);
		swapLength(dvd1,dvd2);
		swapCost(dvd1,dvd2);
	}
	
}
package hust.soict.hedspi.lab01;

import javax.swing.JOptionPane;

public class FirstDegreeSysEq {
     public static void main(String[] args){
        double a11, a12, a21, a22, b1, b2, D, D1, D2;
        String notif2 = "𝑎11𝑥1 + 𝑎12𝑥2 = 𝑏1\n𝑎21𝑥1 + 𝑎22𝑥2 = 𝑏2"; 
        a11 = Double.parseDouble(JOptionPane.showInputDialog(notif2 + "\nEnter a11: "));
        a12 = Double.parseDouble(JOptionPane.showInputDialog(notif2 + "\nEnter a12: "));
        a21 = Double.parseDouble(JOptionPane.showInputDialog(notif2 + "\nEnter a21: "));
        a22 = Double.parseDouble(JOptionPane.showInputDialog(notif2 + "\nEnter a22: "));
        b1 = Double.parseDouble(JOptionPane.showInputDialog(notif2 + "\nEnter b1: "));
        b2 = Double.parseDouble(JOptionPane.showInputDialog(notif2 + "\nEnter b2: "));


        D = a11*a22 - a21*a12;
        D1 = b1*a22 - b2*a12;
        D2 = a11*b2 - a21*b1;
        if(D == 0)
        {
            if(D1 == D2 && D1 == 0){
                JOptionPane.showMessageDialog(null, "The system has infinitely many solutions!");
            }else {
                JOptionPane.showMessageDialog(null, "The system has no solution!");
            }
        } else {
            JOptionPane.showMessageDialog(null, "The system has a unique solution (𝑥1, 𝑥2) = (" + D1/D + ", " + D2/D + ")");
        }
     } }
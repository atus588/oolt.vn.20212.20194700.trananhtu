package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.Scanner;

public class Book extends Media {
	
	private ArrayList<String> authors = new ArrayList<String>();

	public void createBook() {
		super.createMedia();
		Scanner sc = new Scanner(System.in);
		int authNum;
		System.out.println("Enter numbers of authors: ");
		authNum = sc.nextInt();
		for(int i=0;i<authNum;i++) {
			Scanner sc2 = new Scanner(System.in);
			String temp;
			System.out.println("Enter author name: ");
			temp = sc2.nextLine();
			this.addAuthor(temp);
		}
	}
	public ArrayList<String> getAuthors() {
		return authors;
	}


	public void setAuthors(ArrayList<String> authors) {
		this.authors = authors;
	}

	public void addAuthor(String authorName) {
		if(authors.contains(authorName)==false) {
			authors.add(authorName);
			System.out.println("Sucessfully add author: "+authorName);
			}
		else System.out.println("Same author name existed!");
	}
	public void removeAuthor(String authorName) {
		if(authors.contains(authorName)==false) 
			System.out.println("That author not in author list!");
		else {
			authors.remove(authorName);
			System.out.println("Sucessfully remove author: "+authorName);
		}
	}


	public Book() {
		// TODO Auto-generated constructor stub
	}
	public Book(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	public Book(String title, String category,float cost, ArrayList<String> authors){
		super(title, category, cost);
		this.authors = authors;
		//TODO: check author condition
		}
	public String toString() {
		return super.toString() +" - Book";
	}

}

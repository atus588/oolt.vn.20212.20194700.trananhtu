package hust.soict.hedspi.aims.media;

import java.util.Scanner;

public class DigitalVideoDisc extends Media {
	private String director;
	private int length;
	
	public void createDigitalVideoDisc() {
		super.createMedia();
		Scanner sc = new Scanner(System.in);
		System.out.println("Input Director: ");
		this.director = sc.nextLine();
		System.out.println("Input the length: ");
		this.length = sc.nextInt();
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	
	public boolean search(String title) {
		String searchStr = this.getTitle();
		boolean searchResult = searchStr.toLowerCase().contains(title.toLowerCase()); 
		return searchResult;
	}
	public DigitalVideoDisc() {

	}
	public DigitalVideoDisc(String title) {
		super(title);
	}
	public DigitalVideoDisc(String title, String category,float cost, String director, int length) {
		super(title, category, cost);
		this.director = director;
		this.length = length;
	}
	
	public String toString() {
		return super.toString() + " - Director: " + this.director + " - Length: " + this.length+" - DVD";
	}
}

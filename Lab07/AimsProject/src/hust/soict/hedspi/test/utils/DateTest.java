package hust.soict.hedspi.test.utils;
import java.util.Scanner;

import hust.soict.hedpsi.aims.utils.DateUtils;
import hust.soict.hedpsi.aims.utils.MyDate;

public class DateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyDate Date = new MyDate();
//		MyDate blankDay = new MyDate();
//		MyDate intDay = new MyDate(13,7,2001);
//		Scanner keyboard = new Scanner(System.in);
//		System.out.println("Enter a date: (test MyDate(String date) ) ");
//		String date = keyboard.nextLine();
//		MyDate strDay = new MyDate(date);
		
//		Date.accept();
//		Date.printDate();
//		System.out.println(blankDay.getYear()+"-"+blankDay.getMonth()+"-"+blankDay.getDay()+" -> MyDate()");
//		System.out.println(intDay.getYear()+"-"+intDay.getMonth()+"-"+intDay.getDay()+" -> MyDate(13,7,2001)");
//		System.out.println(strDay.getYear()+"-"+strDay.getMonth()+"-"+strDay.getDay()+" -> MyDate(string date)");
//		System.out.println(Date.getYear()+"-"+Date.getMonth()+"-"+Date.getDay()+" -> accept()");
		Date.print();
		MyDate intDay1 = new MyDate(13,7,2001);
		MyDate intDay2 = new MyDate(5,8,2001);
		MyDate intDay3 = new MyDate(13,7,2001);
		MyDate intDay4 = new MyDate(14,7,2001);
		MyDate intDay5 = new MyDate(13,7,1999);
		
		DateUtils testDate = new DateUtils();
		System.out.println(testDate.compareDate(intDay1, intDay2));
		System.out.println(testDate.compareDate(intDay1, intDay3));
		System.out.println(testDate.compareDate(intDay1, intDay4));
		System.out.println(testDate.compareDate(intDay1, intDay5));
		
		testDate.sortDate(intDay5,intDay4,intDay3,intDay2,intDay1);
		
	}

}

package hust.soict.hedspi.garbage;
import java.io.File;  // Import the File class
import java.io.FileNotFoundException;  // Import this class to handle errors
import java.util.Scanner; // Import the Scanner class to read text files


public class GarbageCreator {
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
	    String garbage = "";
	    try {
	      File myObj = new File("C:\\OOP project\\Lab03\\LongTextFile.txt");
	      Scanner myReader = new Scanner(myObj);
	      while (myReader.hasNextLine()) {
	        String data = myReader.nextLine();
	        garbage += data;
	      }
	      myReader.close();
	    } catch (FileNotFoundException e) {
	      System.out.println("An error occurred.");
	      e.printStackTrace();
	    } finally {
//	    	System.out.println(garbage);
	    	System.out.println("Time consume: " +(System.currentTimeMillis()-start));
	    }
	    
	  }
}

import java.util.Scanner;
public class DateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyDate Date = new MyDate();
		MyDate blankDay = new MyDate();
		MyDate intDay = new MyDate(13,7,2001);
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter a date: (test MyDate(String date) ) ");
		String date = keyboard.nextLine();
		MyDate strDay = new MyDate(date);
		
		Date.accept();
		Date.printDate();
		System.out.println(blankDay.getYear()+"-"+blankDay.getMonth()+"-"+blankDay.getDay()+" -> MyDate()");
		System.out.println(intDay.getYear()+"-"+intDay.getMonth()+"-"+intDay.getDay()+" -> MyDate(13,7,2001)");
		System.out.println(strDay.getYear()+"-"+strDay.getMonth()+"-"+strDay.getDay()+" -> MyDate(string date)");
		System.out.println(Date.getYear()+"-"+Date.getMonth()+"-"+Date.getDay()+" -> accept()");
	}

}

package hust.soict.hedspi.aims;

import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.order.Order;

public class DiskTest {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Create a new dvd object and set the fields
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		
		DigitalVideoDisc dvd4 = new DigitalVideoDisc("Harry Porter");
		dvd3.setCategory("Magic Fiction");
		dvd3.setCost(27.99f);
		dvd3.setDirector("Somebody");
		dvd3.setLength(120);
		
		Order o1 = Order.createOrder();
		o1.addDigitalVideoDisc(dvd1);
		o1.addDigitalVideoDisc(dvd2);
		o1.addDigitalVideoDisc(dvd1,dvd4,dvd3);
		o1.addDigitalVideoDisc(dvd1,dvd2,dvd3);
		o1.addDigitalVideoDisc(dvd1,dvd2,dvd3);
		o1.addDigitalVideoDisc(dvd1,dvd2,dvd3);
		
		System.out.println(dvd1.search("lion"));
		System.out.println(dvd4.search("Harry Porter"));
		System.out.println(dvd1.search("harry"));

		o1.newPrintOrder();
		o1.getALuckyItem();
		
		
		
	}
}

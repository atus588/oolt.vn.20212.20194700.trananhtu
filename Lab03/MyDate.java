import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;
public class MyDate {
	private int day, month, year;

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public MyDate() {
		super();
		LocalDate currentDate = LocalDate.now();
		this.day = currentDate.getDayOfMonth();
		this.month = currentDate.getMonthValue();
		this.year = currentDate.getYear();
	}
	
	public MyDate(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public MyDate(String date) {
		super();
		// split string by space, save in array
		String[] splited = date.split("\\s+");
		// year
		Integer yearNum = Integer.valueOf(splited[2]);
		this.year = yearNum;
		// month
		if (splited[0].equals("January")==true) {this.month=1;}
		else if (splited[0].equals("February")==true) {this.month=2;}
		else if (splited[0].equals("March")==true) {this.month=3;}
		else if (splited[0].equals("April")==true) {this.month=4;}
		else if (splited[0].equals("May")==true) {this.month=5;}
		else if (splited[0].equals("June")==true) {this.month=6;}
		else if (splited[0].equals("July")==true) {this.month=7;}
		else if (splited[0].equals("August")==true) {this.month=8;}
		else if (splited[0].equals("September")==true) {this.month=9;}
		else if (splited[0].equals("October")==true) {this.month=10;}
		else if (splited[0].equals("November")==true) {this.month=11;}
		else if (splited[0].equals("December")==true) {this.month=12;}
		else System.out.println("Wrong month name");
		// day
		String dayStr = splited[1].replaceAll("[^0-9]", "");
		Integer dayNum = Integer.valueOf(dayStr);
		this.day = dayNum;
			
	}
	public void accept() {
		try (Scanner keyboard = new Scanner(System.in)) {
			System.out.println("Input the date: (test accept() ) ");
			String date = keyboard.nextLine();
			// split string by space, save in array
			String[] splited = date.split("\\s+");
			// year
			Integer yearNum = Integer.valueOf(splited[2]);
			this.year = yearNum;
			// month
			if (splited[0].equals("January")==true) {this.month=1;}
			else if (splited[0].equals("February")==true) {this.month=2;}
			else if (splited[0].equals("March")==true) {this.month=3;}
			else if (splited[0].equals("April")==true) {this.month=4;}
			else if (splited[0].equals("May")==true) {this.month=5;}
			else if (splited[0].equals("June")==true) {this.month=6;}
			else if (splited[0].equals("July")==true) {this.month=7;}
			else if (splited[0].equals("August")==true) {this.month=8;}
			else if (splited[0].equals("September")==true) {this.month=9;}
			else if (splited[0].equals("October")==true) {this.month=10;}
			else if (splited[0].equals("November")==true) {this.month=11;}
			else if (splited[0].equals("December")==true) {this.month=12;}
			else System.out.println("Wrong month name");
			// day
			String dayStr = splited[1].replaceAll("[^0-9]", "");
			Integer dayNum = Integer.valueOf(dayStr);
			this.day = dayNum;
	        
				
		}
	}
	public void printDate() {
		LocalDate currentDate = LocalDate.now();
		System.out.println(currentDate + "-> printDate()");
	}
}

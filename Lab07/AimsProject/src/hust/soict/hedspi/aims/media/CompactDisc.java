package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.hedspi.aims.track.Playable;
import hust.soict.hedspi.aims.track.Track;

public class CompactDisc extends Disc implements Playable {
	private String artist;
	private ArrayList<Track> track = new ArrayList<Track>();
	
	
	public void addTrack(Track track) {
		boolean check = false;
		for (int i = 0;i<this.track.size();i++) {
			if(this.track.get(i).equals(track)) check = true;
		}
		if (check==true) {
			System.out.println("This track already in the list!");
		} else {
			this.track.add(track);
			System.out.println("Sucessfully added "+track.getTitle());
		}
	}
	public void addTrack() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of tracks: ");
		int num = sc.nextInt();
		for (int i =0;i<num;i++) {
			Track tmpTrack = Track.createTrack();
			this.addTrack(tmpTrack);
		}
	}
	public void removeTrack(Track track) {
		boolean check = true;
		for(int i=0;i<this.track.size();i++) {
			if(this.track.get(i).getTitle() != track.getTitle()) {
				this.track.remove(i);
				System.out.println("Sucessfully removed "+track.getTitle());
			}
			else check=false;
		}
		if (check==false) {
			System.out.println("This track is not in the list!");
		}
	}
	public int getLength() {
		int length = 0;
		for(int i=0;i<this.track.size();i++) {
			length += this.track.get(i).getLength();
		}
		return length;
	}
	public void play() {
		if (this.track.size()==0) {
			System.out.println("This CD has no track!");
		} else {
			for(int i=0;i<this.track.size();i++) {
				this.track.get(i).play();
			}
		}
	}
	
	//--------------------------------------------------------------------
	public void createCompactDisc() {
		super.createDisc();
	}
		
	public String getArtist() {
		return this.artist;
	}
	
	public CompactDisc(String title) {
		super(title);
	}
	
	public CompactDisc(String title, String category,float cost, String director, int length) {
		super(title, category, cost, director, length);
	}
	
	public CompactDisc(String title, String category,float cost, String director, int length,
			String artist) {
		super(title, category, cost, director, length);
		this.artist = artist;
	}
	
	public CompactDisc(String title, String category,float cost, String director, int length,
			String artist,ArrayList<Track> track) {
		super(title, category, cost, director, length);
		this.artist = artist;
		this.track = track;
	}

	public CompactDisc() {
		// TODO Auto-generated constructor stub
		
	}
	public String toString() {
		return super.toString() +" - CD";
	}
}

package hust.soict.hedspi.aims.track;

public interface Playable {
	public void play();
}

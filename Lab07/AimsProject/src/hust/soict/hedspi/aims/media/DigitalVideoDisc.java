package hust.soict.hedspi.aims.media;

import hust.soict.hedspi.aims.track.Playable;

public class DigitalVideoDisc extends Disc implements Playable {
	
	public void createDigitalVideoDisc() {
		super.createDisc();
	}

	public boolean search(String title) {
		String searchStr = this.getTitle();
		boolean searchResult = searchStr.toLowerCase().contains(title.toLowerCase()); 
		return searchResult;
	}
	public DigitalVideoDisc() {

	}
	public DigitalVideoDisc(String title) {
		super(title);
	}
	public DigitalVideoDisc(String title, String category,float cost, String director, int length) {
		super(title, category, cost,director, length);
	}
	
	public String toString() {
		return super.toString() + "- DVD";
	}
//---------------------------------------------------------------------
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}

}

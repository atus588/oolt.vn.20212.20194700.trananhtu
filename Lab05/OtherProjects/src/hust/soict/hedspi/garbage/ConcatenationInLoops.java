package hust.soict.hedspi.garbage;

import java.util.Random;

public class ConcatenationInLoops {
	public static void main(String[] args) {
		Random r = new Random(123);
		long start = System.currentTimeMillis();
		String s = "";
		for (int i = 0; i < 65536; i++)
			s += r.nextInt(2);
		System.out.println(System.currentTimeMillis()-start);
		
		r = new Random(123);
		start = System.currentTimeMillis();
		StringBuilder strBuilder = new StringBuilder();
		for (int i = 0; i < 65536; i ++)
			strBuilder.append(r.nextInt(2));
		s = strBuilder.toString();
		System.out.println(System.currentTimeMillis()-start);
		
		r = new Random(123);
		start = System.currentTimeMillis();
		StringBuffer strBuffer = new StringBuffer();
		for (int i = 0; i < 65536; i ++)
			strBuffer.append(r.nextInt(2));
		s = strBuffer.toString();
		System.out.println(System.currentTimeMillis()-start);
		
	}
}

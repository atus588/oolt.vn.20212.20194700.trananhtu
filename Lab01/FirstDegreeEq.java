import javax.swing.JOptionPane;

public class FirstDegreeEq {
    public static void main(String[] args) {
            String stra, strb;
            stra = JOptionPane.showInputDialog(null, "Please input a: ");
            float a = Float.parseFloat(stra);
            strb = JOptionPane.showInputDialog(null, "Please input b: ");
            float b = Float.parseFloat(strb);
            float x = -b/a;
            JOptionPane.showMessageDialog(null,x,"The solution is:", JOptionPane.INFORMATION_MESSAGE);

    }
}

package hust.soict.hedspi.aims.media;

import java.util.ArrayList;

public class BookTest {

	public static void main(String[] args) {
		
		Book b1 = new Book();
		b1.setTitle("Sach giao khoa Toan");
		b1.addAuthor("Tien si nao day");
		b1.addAuthor("Giao su Toan");
		b1.addAuthor("ABCDE");
		System.out.println(b1.getAuthors());
		b1.addAuthor("ABCDE");
		b1.removeAuthor("ABCDE");
		System.out.println(b1.getAuthors());
		b1.removeAuthor("ABCDE");

		System.out.println(b1.getTitle());
		System.out.println(b1.getCategory());
		System.out.println(b1.getCost());
		System.out.println(b1.getAuthors());

	}

}

package hust.soict.hedpsi.aims.utils;
import java.time.LocalDate;
import java.util.Scanner;
public class MyDate {
	private int day, month, year;

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public MyDate() {
		super();
		LocalDate currentDate = LocalDate.now();
		this.day = currentDate.getDayOfMonth();
		this.month = currentDate.getMonthValue();
		this.year = currentDate.getYear();
	}
	
	public MyDate(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public MyDate(String date) {
		super();
		// split string by space, save in array
		String[] splited = date.split("\\s+");
		// year
		Integer yearNum = Integer.valueOf(splited[2]);
		this.year = yearNum;
		// month
		if (splited[0].equals("January")==true) {this.month=1;}
		else if (splited[0].equals("February")==true) {this.month=2;}
		else if (splited[0].equals("March")==true) {this.month=3;}
		else if (splited[0].equals("April")==true) {this.month=4;}
		else if (splited[0].equals("May")==true) {this.month=5;}
		else if (splited[0].equals("June")==true) {this.month=6;}
		else if (splited[0].equals("July")==true) {this.month=7;}
		else if (splited[0].equals("August")==true) {this.month=8;}
		else if (splited[0].equals("September")==true) {this.month=9;}
		else if (splited[0].equals("October")==true) {this.month=10;}
		else if (splited[0].equals("November")==true) {this.month=11;}
		else if (splited[0].equals("December")==true) {this.month=12;}
		else System.out.println("Wrong month name");
		// day
		String dayStr = splited[1].replaceAll("[^0-9]", "");
		Integer dayNum = Integer.valueOf(dayStr);
		this.day = dayNum;
			
	}
	public void accept() {
		try (Scanner keyboard = new Scanner(System.in)) {
			System.out.println("Input the date: (test accept() ) ");
			String date = keyboard.nextLine();
			// split string by space, save in array
			String[] splited = date.split("\\s+");
			// year
			Integer yearNum = Integer.valueOf(splited[2]);
			this.year = yearNum;
			// month
			if (splited[0].equals("January")==true) {this.month=1;}
			else if (splited[0].equals("February")==true) {this.month=2;}
			else if (splited[0].equals("March")==true) {this.month=3;}
			else if (splited[0].equals("April")==true) {this.month=4;}
			else if (splited[0].equals("May")==true) {this.month=5;}
			else if (splited[0].equals("June")==true) {this.month=6;}
			else if (splited[0].equals("July")==true) {this.month=7;}
			else if (splited[0].equals("August")==true) {this.month=8;}
			else if (splited[0].equals("September")==true) {this.month=9;}
			else if (splited[0].equals("October")==true) {this.month=10;}
			else if (splited[0].equals("November")==true) {this.month=11;}
			else if (splited[0].equals("December")==true) {this.month=12;}
			else System.out.println("Wrong month name");
			// day
			String dayStr = splited[1].replaceAll("[^0-9]", "");
			Integer dayNum = Integer.valueOf(dayStr);
			this.day = dayNum;
	        
				
		}
	}
	public void printDate() {
		LocalDate currentDate = LocalDate.now();
		System.out.println(currentDate + "-> printDate()");
	}
	public void getOrderedDate() {
		System.out.println(this.day+"/"+this.month+"/"+this.year);
	}
	public void print() {
		LocalDate currentDate = LocalDate.now();
		int printDayInt = currentDate.getDayOfMonth();
		String dayTail = "";
		if (printDayInt == 1 || printDayInt == 11 || printDayInt == 31) {dayTail = "st ";}
		else if (printDayInt == 2 || printDayInt == 12) {dayTail = "nd ";}
		else if (printDayInt == 3 || printDayInt == 13) {dayTail = "rd ";} else {dayTail = "th ";}
		
		int printMonthInt = currentDate.getMonthValue();
		String strMonth = "";
		if (printMonthInt == 1) {strMonth = "January ";} else if (printMonthInt == 2) {strMonth = "February ";}
		else if (printMonthInt == 3) {strMonth = "March ";} else if (printMonthInt == 4) {strMonth = "April ";}
		else if (printMonthInt == 5) {strMonth = "May ";} else if (printMonthInt == 6) {strMonth = "June ";}
		else if (printMonthInt == 7) {strMonth = "July ";} else if (printMonthInt == 8) {strMonth = "August ";}
		else if (printMonthInt == 9) {strMonth = "September ";} else if (printMonthInt == 10) {strMonth = "October ";}
		else if (printMonthInt == 11) {strMonth = "November ";} else if (printMonthInt == 12) {strMonth = "December ";}
		
		System.out.println(strMonth+printDayInt+dayTail+currentDate.getYear());
	}
	public void printProperly(MyDate date) {
		int printDayInt = date.getDay();
		String dayTail = "";
		if (printDayInt == 1 || printDayInt == 11 || printDayInt == 31) {dayTail = "st ";}
		else if (printDayInt == 2 || printDayInt == 12) {dayTail = "nd ";}
		else if (printDayInt == 3 || printDayInt == 13) {dayTail = "rd ";} else {dayTail = "th ";}
		
		int printMonthInt = date.getMonth();
		String strMonth = "";
		if (printMonthInt == 1) {strMonth = "January ";} else if (printMonthInt == 2) {strMonth = "February ";}
		else if (printMonthInt == 3) {strMonth = "March ";} else if (printMonthInt == 4) {strMonth = "April ";}
		else if (printMonthInt == 5) {strMonth = "May ";} else if (printMonthInt == 6) {strMonth = "June ";}
		else if (printMonthInt == 7) {strMonth = "July ";} else if (printMonthInt == 8) {strMonth = "August ";}
		else if (printMonthInt == 9) {strMonth = "September ";} else if (printMonthInt == 10) {strMonth = "October ";}
		else if (printMonthInt == 11) {strMonth = "November ";} else if (printMonthInt == 12) {strMonth = "December ";}
		
		System.out.println(strMonth+printDayInt+dayTail+date.getYear());
	}
	
//	public MyDate(String day, String month, String year) {
//		super();
//		
//	}
}









import java.util.Scanner;
public class DaysOfMonth {
	public static void main(String args[]) {
		String[] str30Month = {"April","Apr.","Apr","4","June","Jun","6","September","Sept.","9","November","Nov.","Nov","11"};
		String[] strFeb = {"February","Feb.","Feb","2"};
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Input the month");
		String strMonth = keyboard.nextLine();
		System.out.println("Input the year");
		int year = keyboard.nextInt();
		for (int i=0;i<str30Month.length;i++) {
			boolean is30days = strMonth.equals(str30Month[i]);
			if (is30days == true) {
				System.out.println("This month has 30 days");
				System.exit(0);
			}
		}
		for (int i=0;i<strFeb.length;i++) {
			boolean isFeb = strMonth.equals(strFeb[i]);
			if (isFeb == true) {
				if (year % 4==0) {
					if (year % 100 !=0 || year % 400==0) {
					System.out.println("This month has 29 days");
					System.exit(0);}
					else {System.out.println("This month has 28 days");
					System.exit(0);}
				}
				else {
					System.out.println("This month has 28 days");
					System.exit(0);
				}
			}
		}
		System.out.println("This month has 31 days");
		System.exit(0);
	}
}

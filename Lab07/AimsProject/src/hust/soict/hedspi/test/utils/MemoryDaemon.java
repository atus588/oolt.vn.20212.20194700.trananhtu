package hust.soict.hedspi.test.utils;

public class MemoryDaemon implements Runnable {
	long memoryUsed;
	
	public MemoryDaemon() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Runtime rt = Runtime.getRuntime();
		long used;
		 while (true) {
			 used = rt.totalMemory() - rt.freeMemory();
			 if (used != memoryUsed) {
				 System.out.println("\tMemory used = "+used);
				 memoryUsed = used;
			 }
		 }
		
	}
	
	
}

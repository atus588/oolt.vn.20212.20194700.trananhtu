package hust.soict.hedspi.aims.media;

import java.util.Scanner;

public class Media {
	private String title;
	private String category;
	private float cost;
	private int id=0;
	
	public void createMedia() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input the title: ");
		this.title = sc.nextLine();
		System.out.println("Input the category: ");
		this.category = sc.nextLine();
		System.out.println("Input the cost: ");
		this.cost = sc.nextFloat();
	}

	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public float getCost() {
		return cost;
	}



	public void setCost(float cost) {
		this.cost = cost;
	}

	public Media() {
		// TODO Auto-generated constructor stub
	}
	public Media(String title) {
		super();
		this.title = title;
	}


	public Media(String title, String category, float cost) {
		super();
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	
	public String toString() {
		return "ID: "+ this.id + " - Title: " + this.title + " - Category: " + this.category + " - Price: " + this.cost;
	}
}

package hust.soict.hedspi.aims.media;

import java.util.Scanner;

public class CompactDisc extends Media {
	private String director;
	private int length;
	
	public void createCompactDisc() {
		super.createMedia();
		Scanner sc = new Scanner(System.in);
		System.out.println("Input the length: ");
		this.length = sc.nextInt();
	}
	
	public CompactDisc(String title) {
		super(title);
	}
	public CompactDisc(String title, String category,float cost, String director, int length) {
		super(title, category, cost);
		this.director = director;
		this.length = length;
	}
	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public CompactDisc() {
		// TODO Auto-generated constructor stub
		
	}
	public String toString() {
		return super.toString() +" - Length: " + this.length + " - CD";
	}
}

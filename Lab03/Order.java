
public class Order {
	public static final int MAX_NUMBER_ORDERED = 10;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
	int qtyOrdered;
	float total;
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered <10) {
		 itemsOrdered[qtyOrdered]= disc;
		 qtyOrdered++;
		 System.out.println("The item has been added!");
		}
		else System.out.println("The order is full!");
	 }
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int check=0;
		for (int i=0;i<=qtyOrdered;i++) {
			if (itemsOrdered[i]==disc) {
				check=1;
				for (int j=i;j<qtyOrdered;j++) {
					itemsOrdered[j]=itemsOrdered[j+1];
				}
				itemsOrdered[qtyOrdered]=null;
				qtyOrdered--;
				System.out.println("The item has been removed!");
				break;
			}
		}
		if (check == 0) System.out.println("That item not in your order!");	
	}
	public void printOrder() {
		for (int i=0;i<qtyOrdered;i++) {
			System.out.println(itemsOrdered[i]);
		}
	}
	public float totalCost() {
		for (int i=0;i<qtyOrdered;i++) {
			total += itemsOrdered[i].getCost();
		}
		return total;
	}
}

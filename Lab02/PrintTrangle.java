import java.util.Scanner;
public class PrintTrangle {
	public static void main(String args[]) {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Input the height of the triangle");
		int n = keyboard.nextInt();
		int i,j,k;
		for(i=0;i<n;i++) {
			for (j=0;j<n-1-i;j++) {
				System.out.print(" ");
			}
			for (k=0;k<2*i+1;k++) {
				System.out.print("*");
			}
			System.out.print("\n");
		}
	}
}

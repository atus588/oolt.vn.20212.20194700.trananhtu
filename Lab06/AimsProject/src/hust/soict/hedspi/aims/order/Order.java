package hust.soict.hedspi.aims.order;
import java.util.ArrayList;

import hust.soict.hedpsi.aims.utils.MyDate;
import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media;

public class Order {
	public static final int MAX_NUMBER_ORDERED = 10;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	float total;
	
	private MyDate dateOrdered = new MyDate();
	public static final int MAX_LIMITED_ORDERS = 5;
	private static int nbOrders = 0;
	
	private Order() {
		
	}
	
	public static Order createOrder() {
		if (nbOrders >= MAX_LIMITED_ORDERS)
		{
			System.out.println("You have reached maximum orders");
			return null;
		}
		else {
			nbOrders ++;
			return new Order();
		}
	}
	
	public void removeMedia(Media media) {
		boolean found = false;
		for(int i=0;i<itemsOrdered.size();i++) {
			if (itemsOrdered.get(i).equals(media)==true) {
				itemsOrdered.remove(i);
				System.out.println("Sucessfully remove: "+media.getTitle());
				found = true;
				break;
			}
		}
		if (found == false) {
			System.out.println("Cannot find this item in Order: "+media.getTitle());
		}
	}
	public void removeMedia(int ID) {
		boolean found = false;
		for(int i=0;i<itemsOrdered.size();i++) {
			if(itemsOrdered.get(i).getId()==ID) {
				itemsOrdered.remove(i);
				System.out.println("Sucessfully remove items with ID: "+ID);
				found = true;
				break;
			}
		}
		if (found==false) System.out.println("No items with ID: "+ID);
	}
	public void addMedia(Media media) {
		if (itemsOrdered.size()<MAX_NUMBER_ORDERED) {
			itemsOrdered.add(media);
			System.out.println("Sucessfully add: "+media.getTitle());
		}
		else {
			System.out.println("Max items reached! Cannot add: "+media.getTitle());
		}
	}	
	
	public float totalCost() {
		for (int i=0;i<itemsOrdered.size();i++) {
			total += itemsOrdered.get(i).getCost();
		}
		return total;
	}
	public void newPrintOrder() {
		System.out.println("*************Order****************");
		System.out.print("Date: ");dateOrdered.getOrderedDate();
		System.out.println("Ordered Items");
		for (int i=0; i<itemsOrdered.size();i++) {
			System.out.println(itemsOrdered.get(i).toString());
		}
		System.out.println("Total cost: "+totalCost());
		System.out.println("**********************************");
		
	}
	
	public void getALuckyItem() {
		this.total = this.totalCost();
		int randRange = itemsOrdered.size() - 1;
		int randNum = (int)(Math.random()*randRange);
		System.out.println("The lucky items is: "+(randNum+1)+" - "+itemsOrdered.get(randNum).getTitle());
		System.out.println("New total cost: "+ (this.total - itemsOrdered.get(randNum).getCost()));
	}
	public int getSize() {
		return this.itemsOrdered.size();
	}
	
}
